package executors

import (
	"os"
	"encoding/base64"                                                                                                                                                                                 
    "crypto/aes"                                                                                                                                                                                            
	"crypto/cipher"
	"crypto/rand"
	"strings"
	"io"
)

func DecryptProjectToken(token string) ([]string, error) {
	key := os.Getenv("CM_PROJECT_SECRET")

	data, err := base64.StdEncoding.DecodeString(token)
	if err != nil {
		return nil, err
	}
	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		return nil, err
	}
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return nil, err
	}
	nonceSize := gcm.NonceSize()
	nonce, ciphertext := data[:nonceSize], data[nonceSize:]
	plaintext, err := gcm.Open(nil, nonce, ciphertext, nil)
	if err != nil {
		return nil, err
	}

	return strings.Split(string(plaintext),","), nil
}

func EncryptProjectToken(data string) (string, error) {
	key := os.Getenv("CM_PROJECT_SECRET")

	block, err := aes.NewCipher([]byte(key))
	if err != nil {
		return "", err
	}
	gcm, err := cipher.NewGCM(block)
	if err != nil {
		return "", err
	}
	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return "", err
	}
	ciphertext := gcm.Seal(nonce, nonce, []byte(data), nil)
	return base64.StdEncoding.EncodeToString(ciphertext), nil
}