package executors

import (
	"testing"
	"os"
)

func Test(t *testing.T) {
	os.Setenv("STATS_SERVER", "http://woolf.mitre.org")
	host, err := GetGPUHost("", 1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(host)
}