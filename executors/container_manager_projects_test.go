package executors

import (
	"testing"
	"os"
	"strings"
)

func TestToken(t *testing.T) {
	os.Setenv("CM_PROJECT_SECRET", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
	inProjects := "cv"
	token, err := EncryptProjectToken(inProjects)
	t.Log(token)
	if err != nil {
		t.Fatal(err)
	}
	outProjectsArr, err := DecryptProjectToken(token)
	if err != nil {
		t.Fatal(err)
	}
	outProjectsStr := strings.Join(outProjectsArr, ",")
	if outProjectsStr != inProjects {
		t.Fatal("In projects do not match out projects")
	}
}